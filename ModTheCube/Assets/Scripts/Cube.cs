﻿using UnityEngine;

public class Cube : MonoBehaviour
{
    /**************************************************************************************************************************************
     *************************************************** Following are the things that this class does: ***********************************
     ************************************-> Initializes the variables in the Start() Method ***********************************************
     *********************************** -> GetRGBAValues() Called in Update() calls IncrementRGBAValues() & then return rgbaValues *******
     *********************************** -> IncrementRGBAValues() increments RGBA Values so colour changes over time **********************
     ****************************************************That's all folks******************************************************************
     **************************************************************************************************************************************/

    private float rgbaOffset;
    [SerializeField] private float rotationSpeed;
    private int randomRGBAIndex;
    private Material material;

    [SerializeField] private CubeDataSO cubeDataSO;
    [SerializeField] private float[] rgbaValues;
    [SerializeField] private MeshRenderer cubeRenderer;
    
    private void Start()
    {
        randomRGBAIndex = Random.Range(0 , 3); //This will cause cube colour to change to one of the 3 primitive colours Red, Green & Blue
        rgbaOffset = Random.Range(0.0001f , 0.001f);

        transform.position = cubeDataSO.GetPosition();
        transform.localScale = cubeDataSO.GetScale() * 1.3f;
        
        material = cubeRenderer.material;
        material.color = new Color(0 , 0 , 0 , 1);

        GetRotationSpeed();
    }
    
    private void Update()
    {
        GetRGBAValues();
        material.color = new Color(GetRGBAValues()[0] , GetRGBAValues()[1] , GetRGBAValues()[2] , GetRGBAValues()[3]);
        transform.Rotate(cubeDataSO.GetAngle() * rotationSpeed * Time.deltaTime , 0.0f , 0.0f);
    }

    private float GetRotationSpeed()
    {
        rotationSpeed = Random.Range(cubeDataSO.GetRotationSpeed() , cubeDataSO.GetRotationSpeed() * 3);
        return rotationSpeed;
    }

    private float[] GetRGBAValues()
    {   
        IncrementRGBAValues();
        return rgbaValues;
    }

    private void IncrementRGBAValues()
    {
        if(rgbaValues[randomRGBAIndex] < 1.0f)
        {
            rgbaValues[randomRGBAIndex] += rgbaOffset;
        }

        if(rgbaValues[randomRGBAIndex] >= 1.0f)
        {
            rgbaValues[randomRGBAIndex] = 1.0f;
        }
    }
}
