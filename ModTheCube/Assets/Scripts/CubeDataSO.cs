﻿using UnityEngine;

[CreateAssetMenu]
public class CubeDataSO : ScriptableObject
{
    /**************************************************************************************************************************************
     *************************************** Following are the things that this Scriptable Object does: ***********************************
     ** -> Contains private [SerializeField] variables that can be modified in the inspector **********************************************
     ** -> Contains Get Methods that are public so the Cube can get the necessary values while keeping the variables private hence secure *
     ** -> The main purpose of this SO is to keep static data separate from the MonoBehaviour *********************************************
     ** -> Up Next will be using GetScale(), cube scale will be different at every start **************************************************
     ****************************************************That's all folks******************************************************************
     **************************************************************************************************************************************/

    private Vector3 scale = Vector3.one;

    [SerializeField] private float angle;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private Vector3 position;

    public float GetAngle()
    {
        return angle;
    }

    public float GetRotationSpeed()
    {
        return rotationSpeed;
    }

    public Vector3 GetPosition()
    {
        return position;
    }

    public Vector3 GetScale()
    {
        return scale;
    }
}
