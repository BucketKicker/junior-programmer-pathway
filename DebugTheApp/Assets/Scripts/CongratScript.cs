﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CongratScript : MonoBehaviour
{
    private float rotatingSpeed;
    private float timeToNextText;
    private int currentText;

    [SerializeField] private List<string> textsToDisplay;
    [SerializeField] private ParticleSystem sparksParticles;
    [SerializeField] private TextMeshPro textToDisplay;
    
    private void Start()
    {
        timeToNextText = 0.0f;
        currentText = 0;
        
        rotatingSpeed = 1.0f;

        textsToDisplay.Add("Congratulation");
        textsToDisplay.Add("All Errors Fixed");

        textToDisplay.text = textsToDisplay[0];
        
        sparksParticles.Play();
    }
    private void Update()
    {
        timeToNextText += Time.deltaTime;

        if(timeToNextText > 1.5f)
        {
            timeToNextText = 0.0f;
            
            currentText++;

            if(currentText >= textsToDisplay.Count)
            {
                currentText = 0;
            }

            textToDisplay.text = textsToDisplay[currentText];
        }
    }
}