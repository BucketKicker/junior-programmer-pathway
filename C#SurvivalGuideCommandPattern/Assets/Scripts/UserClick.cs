using UnityEngine;

public class UserClick : MonoBehaviour
{
    private void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        // Left Click
        // Cast a ray
        // Detect a cube
        // Assign random colour

        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray , out hit))
            {
                if(hit.collider.tag.Equals("Cube"))
                {
                    ICommand clickCommand = new ClickCommand(hit.collider.gameObject , new Color(Random.value , Random.value , Random.value));
                    clickCommand.Execute();
                    CommandManager.Instance.AddCommand(clickCommand); // I couldn't figure this out on my own
                }
            }


        }
    }
}
