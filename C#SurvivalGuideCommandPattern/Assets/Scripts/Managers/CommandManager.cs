using System.Collections;
using System.Collections.Generic;
using System.Linq; // I never used this before
using UnityEngine;

public class CommandManager : MonoBehaviour
{
    // When I press Play, the sequence is playing in an unknown order rather than repeating what I have done exactly, unlike explained in the tutorial so debug this when you have time
    #region Startup & Declarations
    private static CommandManager _instance;

    public static CommandManager Instance
    {
        get
        {
            if(_instance == null)
            {
                Debug.Log("The Command Manager is NULL :( ");
            }

            return _instance;
        }
    }

    private List<ICommand> _commandsBuffer = new List<ICommand>();
    private void Awake()
    {
        _instance = this;
    }
    #endregion


    #region Routines
    public IEnumerator PlayRoutine()
    {
        foreach(ICommand command in _commandsBuffer) // I figured this out on my own
        {
            command.Execute();
            yield return new WaitForSeconds(1.0f);
        }
    }

    public IEnumerator RewindRoutine()
    {
        foreach(ICommand command in Enumerable.Reverse(_commandsBuffer)) // This is totally new to me and loved that I got to learn about this and this is from System.Linq Library
        {
            command.Undo();
            yield return new WaitForSeconds(1.0f);
        }
    }
    #endregion

    //This is the only thing missed so far when I forgot to make the method take in a parameter and the minute the instructor mentioned a parameter, I paused the video and did the rest
    public void AddCommand(ICommand command)
    {
        _commandsBuffer.Add(command);
    }

    #region Buttons
    public void DoneButton()
    {
        var cubes = GameObject.FindGameObjectsWithTag("Cube");

        foreach(var cube in cubes)
        {
            cube.GetComponent<MeshRenderer>().material.color = Color.white;
        }
    }

    public void PlayButton()
    {
        StartCoroutine(PlayRoutine());
    }

    public void ResetButton()
    {
        _commandsBuffer.Clear(); // I figured this out on my own
    }

    public void RewindButton()
    {
        StartCoroutine(RewindRoutine());
    }
    #endregion
}
