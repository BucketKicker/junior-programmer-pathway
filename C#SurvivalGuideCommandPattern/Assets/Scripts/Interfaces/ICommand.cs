public interface ICommand
{
    //Unlike in the classes, if no access modifier specified, these are public by default where in classes, these are private by default
    public void Execute();
    public void Undo();
}
