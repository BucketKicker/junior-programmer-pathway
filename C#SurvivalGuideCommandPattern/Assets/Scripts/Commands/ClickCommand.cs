using UnityEngine;
public class ClickCommand : ICommand
{
    private Color previousColour;

    [SerializeField] private Color cubeColour;
    [SerializeField] private GameObject cubeObj;
    public ClickCommand(GameObject cube , Color colour)
    {
        cubeObj = cube;
        cubeColour = colour;
    }
    public void Execute()
    {
        previousColour = cubeObj.GetComponent<MeshRenderer>().material.color;
        cubeObj.GetComponent<MeshRenderer>().material.color = cubeColour;
    }

    public void Undo()
    {
        cubeObj.GetComponent<MeshRenderer>().material.color = previousColour;
    }
}
