﻿using UnityEngine;

[CreateAssetMenu]
public class EnemyDataSO : ScriptableObject
{
    [SerializeField] private float incrementValue;
    [SerializeField] private float moveSpeed;

    public float GetMoveSpeed()
    {
        return moveSpeed;
    }

    public float GetIncrementValue()
    {
        return incrementValue;
    }

    public void IncrementMoveSpeed(float speed)
    {
        moveSpeed += speed;
    }
}
