﻿using UnityEngine;

public class RepeatBackgroundX : MonoBehaviour
{
    private float backgroundWidth;
    private Vector3 startPos;

    [SerializeField] private BoxCollider backgroundCollider;
    [SerializeField] private MiscDataSO miscDataSO;

    private void Start()
    {
        backgroundWidth = backgroundCollider.size.x / 2;
        startPos = transform.position;    
    }

    private void Update()
    {
        if(transform.position.x < startPos.x - backgroundWidth)
        {
            transform.position = startPos;
        }
    }
}


