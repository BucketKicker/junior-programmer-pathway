﻿using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
    [SerializeField] private MeshRenderer playerRenderer;
    [SerializeField] private MiscDataSO miscDataSO;
    [SerializeField] private PlayerDataSO playerDataSO;
    [SerializeField] private ParticleSystem[] particlesPrefabs;
    [SerializeField] private Rigidbody playerRb;
    [SerializeField] private SoundManager soundManager;
    [SerializeField] private SoundsDataSO soundsDataSO;

    private void Start()
    {
        miscDataSO.SetGameOver(false);

        playerDataSO.SetGravityModifier(1.5f);

        Physics.gravity *= playerDataSO.GetGravityModifier();

        if(transform.position.y < playerDataSO.GetMaxYPos())
        {
            playerRb.AddForce(Vector3.up * playerDataSO.GetFloatForce()  * Time.deltaTime , ForceMode.Impulse);
        }
    }

    private void Update()
    {
        PlayerInput();
    }

    private void OnCollisionEnter(Collision other)
    {
        if(!miscDataSO.IsGameOver())
        {
            if(other.gameObject.CompareTag("Bomb"))
            {
                particlesPrefabs[0].Play();
                soundManager.GetInGameSoundsSource().PlayOneShot(soundsDataSO.GetExplosionSound() , 1.0f);
                miscDataSO.SetGameOver(true);
                Destroy(other.gameObject);
                playerDataSO.SetGravityModifier(0);
                playerRenderer.enabled = false;
            } 

            else if(other.gameObject.CompareTag("Ground"))
            {
                soundManager.GetInGameSoundsSource().PlayOneShot(soundsDataSO.GetPlayerSound() , 1.0f);
                playerRb.AddForce(Vector3.up * playerDataSO.GetFloatForce()  * Time.deltaTime , ForceMode.Impulse);
            }

            else if(other.gameObject.CompareTag("Money"))
            {
                particlesPrefabs[1].Play();
                soundManager.GetInGameSoundsSource().PlayOneShot(soundsDataSO.GetMoneySound() , 1.0f);
                Destroy(other.gameObject);
            }
        }
    }

    private void PlayerInput()
    {
        if(Input.GetKeyDown(KeyCode.Space) && !miscDataSO.IsGameOver())
        {
            if(transform.position.y < playerDataSO.GetMaxYPos())
            {
                playerRb.AddForce(Vector3.up * playerDataSO.GetFloatForce() * Time.deltaTime , ForceMode.Impulse);
            }
            else
            {
                return;
            }
        }
    }
}
