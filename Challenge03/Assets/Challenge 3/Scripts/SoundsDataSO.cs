﻿using UnityEngine;

[CreateAssetMenu]
public class SoundsDataSO : ScriptableObject
{
    [SerializeField] private AudioClip explosionSound;
    [SerializeField] private AudioClip moneySound;
    [SerializeField] private AudioClip playerSound;

    public AudioClip GetExplosionSound()
    {
        return explosionSound;
    }

    public AudioClip GetMoneySound()
    {
        return moneySound;
    }

    public AudioClip GetPlayerSound()
    {
        return playerSound;
    }
}
