﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private AudioClip[] backgroundMusicClips;
    [SerializeField] private AudioSource inGameSoundsSource;
    [SerializeField] private AudioSource backgroundMusicSource;
    [SerializeField] private SoundsDataSO soundsDataSO;

    private void Start()
    {
        int audioClipsIndex = Random.Range(0 , backgroundMusicClips.Length);
        backgroundMusicSource.clip = backgroundMusicClips[audioClipsIndex];
        backgroundMusicSource.Play();
    }

    public AudioSource GetBackgroundMusicSource()
    {
        return backgroundMusicSource;
    }

    public AudioSource GetInGameSoundsSource()
    {
        return inGameSoundsSource;
    }
}
