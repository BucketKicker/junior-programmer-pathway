﻿using UnityEngine;

public class MoveLeftX : MonoBehaviour
{
    [SerializeField] private MiscDataSO miscDataSO;

    private void Update()
    {
        if(!miscDataSO.IsGameOver())
        {
            transform.Translate(Vector3.left * miscDataSO.GetSpeed() * Time.deltaTime , Space.World);
        }

        if(transform.position.x < miscDataSO.GetLeftBoundary() && (gameObject.CompareTag("Bomb") || gameObject.CompareTag("Money")))
        {
            Destroy(gameObject);
        }

    }
}
