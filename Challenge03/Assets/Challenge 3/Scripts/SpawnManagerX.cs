﻿using UnityEngine;

public class SpawnManagerX : MonoBehaviour
{
    [SerializeField] private MiscDataSO miscDataSO;

    private void Start()
    {
        InvokeRepeating("SpawnObjects" , miscDataSO.GetStartDelay() , miscDataSO.GetSpawnInterval());
    }

    private void SpawnObjects()
    {
        Vector3 spawnLocation = new Vector3(miscDataSO.GetSpawnLocationX() , Random.Range(miscDataSO.GetSpawnLocationYMin() , miscDataSO.GetSpawnLocationYMax()) , 0);
        int index = Random.Range(0 , miscDataSO.GetObjectsToSpawn().Length);

        if (!miscDataSO.IsGameOver())
        {
            Instantiate(miscDataSO.GetObjectsToSpawn()[index] , spawnLocation , miscDataSO.GetObjectsToSpawn()[index].transform.rotation);
        }
    }
}
