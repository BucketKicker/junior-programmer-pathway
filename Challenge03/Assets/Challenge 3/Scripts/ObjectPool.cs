﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] private int bombsPoolDepth;
    [SerializeField] private bool canGrow = true;
    //[SerializeField] private UsefulDataSO usefulDataSO;

    private readonly List<GameObject> bombsPool = new List<GameObject>();

    private void Awake()
    {
        for(int i = 0; i < bombsPoolDepth; i++)
        {
            //GameObject pooledBomb = Instantiate(usefulDataSO.GetBalls()[i]);
            //pooledBomb.SetActive(false);
            //bombsPool.Add(pooledBomb);
        }
    }

    public GameObject GetAvailableBomb()
    {
        int ballsIndex = Random.Range(0 , bombsPoolDepth);

        for(int i = 0; i < bombsPoolDepth; i++)
        {
            if(bombsPool[ballsIndex].activeInHierarchy == false)
                return bombsPool[ballsIndex];
        }

        if(canGrow == true)
        {
            //GameObject pooledObject = Instantiate(usefulDataSO.GetBalls()[ballsIndex]);
            //bombsPool.Add(pooledObject);

            //return pooledObject;
            return gameObject; //Delete this after you uncomment the line above
        }

        else
            return null;
    }
}
