﻿using UnityEngine;

[CreateAssetMenu]
public class PlayerDataSO : ScriptableObject
{
    private bool isGrounded;

    [SerializeField] private float floatForce;
    [SerializeField] private float gravityModifier;
    [SerializeField] private float maxYPos;

    public bool IsGrounded()
    {
        return isGrounded;
    }

    public float GetGravityModifier()
    {
        return gravityModifier;
    }

    public float GetFloatForce()
    {
        return floatForce;
    }

    public float GetMaxYPos()
    {
        return maxYPos;
    }

    public void SetGravityModifier(float gravityModifierValue)
    {
       gravityModifier = gravityModifierValue; 
    }

    public void SetGroundedStatus(bool grounded) //Not sure this is the right way to do it Conceptually because the data here is being changed by some other class
    {
        isGrounded = grounded;
    }
}
