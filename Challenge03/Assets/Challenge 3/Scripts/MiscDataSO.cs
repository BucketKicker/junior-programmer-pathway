﻿using UnityEngine;

[CreateAssetMenu]
public class MiscDataSO : ScriptableObject
{
    private bool isGameOver = false;

    [SerializeField] private float bombLeftBoundary;
    [SerializeField] private float moveLeftSpeed;
    [SerializeField] private float spawnManagerStartDelay;
    [SerializeField] private float spawnManagerSpawnInterval;
    [SerializeField] private float spawnManagerSpawnLocationX;
    [SerializeField] private float spawnManagerSpawnLocationYMax;
    [SerializeField] private float spawnManagerSpawnLocationYMin;
    [SerializeField] private GameObject[] objectsToSpawn;

    public bool IsGameOver()
    {
        return isGameOver;
    }

    public float GetLeftBoundary()
    {
        return bombLeftBoundary;
    }

    public float GetSpeed()
    {
        return moveLeftSpeed;
    }

    public float GetStartDelay()
    {
        return spawnManagerStartDelay;
    }

    public float GetSpawnInterval()
    {
        return spawnManagerSpawnInterval;
    }

    public float GetSpawnLocationX()
    {
        return spawnManagerSpawnLocationX;
    }

    public float GetSpawnLocationYMax()
    {
        return spawnManagerSpawnLocationYMax;
    }

    public float GetSpawnLocationYMin()
    {
        return spawnManagerSpawnLocationYMin;
    }

    public GameObject[] GetObjectsToSpawn()
    {
        return objectsToSpawn;
    }

    public void SetGameOver(bool gameOver)
    {
        isGameOver = gameOver;
    }
}
