﻿using Prototype05.Events;
using System;

public class EventsManager
{
    private static event Action GameOverAction;

    private static event Action<int> PlayerClickedAction;

    public static void InvokeEvent(Prototype05Event eventToInvoke)
    {
        switch(eventToInvoke)
        {
            case Prototype05Event.GameOverEvent:
                GameOverAction?.Invoke();
             return;
        }
    }

    public static void InvokeEvent(Prototype05Event eventToInvoke , int changeToValue)
    {
        switch(eventToInvoke)
        {
            case Prototype05Event.PlayerClickedEvent:
                PlayerClickedAction?.Invoke(changeToValue);
            return;
        }
    }

    public static void SubscribeToEvent(Prototype05Event eventToSubscribe , Action actionFunction)
    {
        switch(eventToSubscribe)
        {
            case Prototype05Event.GameOverEvent:
                GameOverAction += actionFunction;
            return;
        }
    }

    public static void SubscribeToEvent(Prototype05Event eventToSubscribe , Action<int> actionFunction)
    {
        switch(eventToSubscribe)
        {
            case Prototype05Event.PlayerClickedEvent:
                PlayerClickedAction += actionFunction;
            return;
        }
    }

    public static void UnsubscribeFromEvent(Prototype05Event eventToSubscribe , Action actionFunction)
    {
        switch(eventToSubscribe)
        {
            case Prototype05Event.GameOverEvent:
                GameOverAction -= actionFunction;
            return;
        }
    }

    public static void UnsubscribeFromEvent(Prototype05Event eventToUnsubscribe, Action<int> actionFunction)
    {
        switch(eventToUnsubscribe)
        {
            case Prototype05Event.PlayerClickedEvent:
                PlayerClickedAction -= actionFunction;
            return;
        }
    }
}
