﻿using UnityEngine;
using UnityEngine.UI;

public class DifficultyButton : MonoBehaviour
{
    [SerializeField] private Button difficultyButton;
    [SerializeField] private GameManager gameManager;
    [SerializeField] private int difficultyLevel;
    private void Start()
    {
        difficultyButton.onClick.AddListener(SetDifficulty);
    }

    private void SetDifficulty()
    {
        gameManager.StartGame(difficultyLevel);
    }
}
