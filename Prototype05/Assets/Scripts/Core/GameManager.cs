﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private bool isGameActive;

    [SerializeField] private int scoreValue;
    [SerializeField] private GameDataSO gameDataSO;
    [SerializeField] private GameObject restartButtonObj;
    [SerializeField] private GameObject startGameItemsObj;
    [SerializeField] private TMP_Text gameOverText;
    [SerializeField] private TMP_Text scoreText;

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    IEnumerator SpawnTarget()
    {
        while(isGameActive)
        {
            yield return new WaitForSeconds(gameDataSO.GetEnemySpawnRate());
            int index = Random.Range(0 , gameDataSO.GetTargetsList().Count);
            Instantiate(gameDataSO.GetTargetsList()[index]);
        }
    }

    private void OnGameOver()
    {
        isGameActive = false;
        gameOverText.gameObject.SetActive(true);
        restartButtonObj.SetActive(true);
        Time.timeScale = 0;
    }

    private void OnPlayerClicked(int changeToValue)
    {
        UpdateScore(changeToValue);
        ResetScoreToZero();
    }

    private void ResetScoreToZero()
    {
        if(scoreValue < 0)
        {
            scoreValue = 0;
            scoreText.text = "Score : " + scoreValue.ToString();
        }
    }

    public int GetScoreValue()
    {
        return scoreValue;
    }

    public TMP_Text GetScoreLabel()
    {
        return scoreText;
    }

    public void RestartButton()
    {
        gameDataSO.ResetEnemySpawnRate();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void StartGame(int difficultyLevel)
    {
        gameDataSO.SetEnemySpawnRate(difficultyLevel);
        isGameActive = true;
        ResetScoreToZero();
        SubscribeToEvents();
        scoreText.text = "Score : " + scoreValue;
        StartCoroutine(SpawnTarget());
        startGameItemsObj.SetActive(false);
        Time.timeScale = 1;
    }

    public void UpdateScore(int incrementValue)
    {
        scoreValue = scoreValue + incrementValue;
        scoreText.text = "Score : " + scoreValue.ToString();
    }

    #region Event Listeners
    private void SubscribeToEvents()
    {
        EventsManager.SubscribeToEvent(Prototype05.Events.Prototype05Event.GameOverEvent, OnGameOver);
        EventsManager.SubscribeToEvent(Prototype05.Events.Prototype05Event.PlayerClickedEvent , OnPlayerClicked);
    }

    private void UnsubscribeFromEvents()
    {
        EventsManager.UnsubscribeFromEvent(Prototype05.Events.Prototype05Event.GameOverEvent , OnGameOver);
        EventsManager.UnsubscribeFromEvent(Prototype05.Events.Prototype05Event.PlayerClickedEvent , OnPlayerClicked);
    }
    #endregion
}