﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameDataSO : ScriptableObject
{
    private float defaultEnemySpawnRate = 1.8f;

    [SerializeField] private float currentEnemySpawnRate;
    [SerializeField] private List<GameObject> targetsList;

    public float GetEnemySpawnRate()
    {
        return currentEnemySpawnRate;
    }
    
    public List<GameObject> GetTargetsList()
    {
        return targetsList;
    }

    public void ResetEnemySpawnRate()
    {
        currentEnemySpawnRate = defaultEnemySpawnRate;
    }

    public void SetEnemySpawnRate(int difficultyLevel)
    {
        currentEnemySpawnRate /= difficultyLevel;
    }
}
