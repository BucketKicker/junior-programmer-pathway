﻿using UnityEngine;

[CreateAssetMenu]
public class TargetDataSO : ScriptableObject
{
    [SerializeField] private int scoreIncrementValue;

    public int GetScoreIncrementValue()
    {
        return scoreIncrementValue;
    }
}
