﻿using UnityEngine;

public class Target : MonoBehaviour
{
    [SerializeField] private float maxSpeed;
    [SerializeField] private float minSpeed;
    [SerializeField] private float torqueRange;
    [SerializeField] private float xPosRange;
    [SerializeField] private float ySpawnPos;
    [SerializeField] private int scoreIncrementValue;
    [SerializeField] private ParticleSystem explosionSystem;
    [SerializeField] private Rigidbody targetBody;

    private void Start()
    {
        SubscribeToEvents();
        targetBody.AddForce(GetRandomForce() , ForceMode.Impulse);
        targetBody.AddTorque(GetRandomTorque() , GetRandomTorque() , GetRandomTorque() , ForceMode.Impulse);
        transform.position = new Vector3(GetRandomSpawnPos().x , -ySpawnPos);
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    private void OnMouseDown()
    {
        EventsManager.InvokeEvent(Prototype05.Events.Prototype05Event.PlayerClickedEvent , scoreIncrementValue);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!gameObject.CompareTag("Bad"))
        {
            EventsManager.InvokeEvent(Prototype05.Events.Prototype05Event.GameOverEvent);
        }
        
        Destroy(gameObject);
    }

    private float GetRandomTorque()
    {
        return Random.Range(-torqueRange , torqueRange);
    }
    
    private Vector3 GetRandomForce()
    {
        return Vector3.up * Random.Range(minSpeed , maxSpeed);
    }

    private Vector3 GetRandomSpawnPos()
    {
        return new Vector3(Random.Range(-xPosRange, xPosRange), ySpawnPos);
    }

    private void OnPlayerClicked(int unusedParameter)
    {
        //TODO If not covered in the tutorial, before moving on to the next lesson, change the particles system to use the destroyed object's 
        //Sprite Renderer
        Instantiate(explosionSystem , transform.position , explosionSystem.transform.rotation);
    }

    #region Event Listeners
    private void SubscribeToEvents()
    {
        EventsManager.SubscribeToEvent(Prototype05.Events.Prototype05Event.PlayerClickedEvent , OnPlayerClicked);
    }

    private void UnsubscribeFromEvents()
    {
        EventsManager.UnsubscribeFromEvent(Prototype05.Events.Prototype05Event.PlayerClickedEvent , OnPlayerClicked);
    }
    #endregion
}
