﻿using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private UsefulDataSO usefulDataSO;
    private void Start()
    {
        InvokeRepeating("SpawnRandomAnimal" , usefulDataSO.GetSpawnStartDelay() , usefulDataSO.GetSpawnInterval());   
    }

    private void SpawnRandomAnimal()
    {
        int animalsIndex = Random.Range(0 , usefulDataSO.GetAnimals().Length);
        Vector3 randomSpawnPos = new Vector3(Random.Range(-usefulDataSO.GetSpawnRangeX() , usefulDataSO.GetSpawnRangeX()) , 0 , usefulDataSO.GetTopBoundary());
        Instantiate(usefulDataSO.GetAnimals()[animalsIndex] , randomSpawnPos , usefulDataSO.GetAnimals()[animalsIndex].transform.rotation);
    }
}
