﻿using UnityEngine;

[CreateAssetMenu]
public class UsefulDataSO : ScriptableObject
{
    [SerializeField] private float bottomZBoundary;
    [SerializeField] private float spawnManagerSpawnStartDelay;
    [SerializeField] private float spawnManagerSpawnInterval;
    [SerializeField] private float spawnManagerSpawnRangeX;
    [SerializeField] private float topZBoundary;
    [SerializeField] private GameObject[] animalPrefabs;

    public GameObject[] GetAnimals()
    {
        return animalPrefabs;
    }
    public float GetBottomBoundary()
    {
        return bottomZBoundary;
    }

    public float GetSpawnRangeX()
    {
        return spawnManagerSpawnRangeX;
    }

    public float GetSpawnInterval()
    {
        return spawnManagerSpawnInterval;
    }

    public float GetSpawnStartDelay()
    {
        return spawnManagerSpawnStartDelay;
    }

    public float GetTopBoundary()
    {
        return topZBoundary;
    }
}
