﻿using UnityEngine;

public delegate void OutOfBoundsHandler();

public class ProjectileController : MonoBehaviour
{
    #region Field Declarations

    public Vector2 projectileDirection;
    public float projectileSpeed;
    public bool isPlayers;

    #endregion

    public event OutOfBoundsHandler ProjectileOutOfBounds;

    #region Movement

    private void Update()
    {
        MoveProjectile();
    }

    private void MoveProjectile()
    {
        transform.Translate(projectileDirection * Time.deltaTime * projectileSpeed , Space.World);

        if(ScreenBounds.OutOfBounds(transform.position))
        {
            if(ProjectileOutOfBounds != null)
            {
                ProjectileOutOfBounds();
            }

            Destroy(gameObject);
        }
    }

    #endregion
}
