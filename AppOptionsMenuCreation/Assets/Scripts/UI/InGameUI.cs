using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameUI : MonoBehaviour
{
    [SerializeField] private GameObject inGameUIObj;
    [SerializeField] private GameObject pauseMenuObj;

    public void PauseButton()
    {
        inGameUIObj.SetActive(false);
        pauseMenuObj.SetActive(true);
        Time.timeScale = 0;
    }

    public void QuitButton()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void RestartButton()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }

    public void ResumeButton()
    {
        inGameUIObj.SetActive(true);
        pauseMenuObj.SetActive(false);
        Time.timeScale = 1;
    }
}
