using UnityEngine;

[CreateAssetMenu]
public class SoundsDataSO : ScriptableObject
{
    [SerializeField] private AudioClip _buttonHoverClip;
    [SerializeField] private AudioClip _buttonPressedClip;

    public AudioClip GetButtonHoverClip()
    {
        return _buttonHoverClip;
    }

    public AudioClip GetButtonPressedClip()
    {
        return _buttonPressedClip;
    }
}
