using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    [SerializeField] private AudioSource inGameSoundsSource;
    [SerializeField] private SoundsDataSO soundsDataSO;
    
    public void PlaySound(AudioClip clipToPlay)
    {
        inGameSoundsSource.clip = clipToPlay;
        inGameSoundsSource.Play();
    }

    public void PauseSound()
    {
        inGameSoundsSource.Pause();
    }
}
