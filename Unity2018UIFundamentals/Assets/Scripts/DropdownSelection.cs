﻿/**
* DropdownSelection.cs grab the value of the dropdown selected
* Author:  Lisa Walkosz-Migliacio  http://evilisa.com  12/18/2018
*/
using UnityEngine;
using UnityEngine.UI;

public class DropdownSelection : MonoBehaviour 
{

    public string selection;

	void Start () 
    {
        Dropdown.OptionData option1 = new Dropdown.OptionData() { text = "Option 1" };
        Dropdown.OptionData option2 = new Dropdown.OptionData() { text = "Option 2" };
        Dropdown.OptionData option3 = new Dropdown.OptionData() { text = "Option 3" };

        GetComponent<Dropdown>().options.Add(option1);
        GetComponent<Dropdown>().options.Add(option2);
        GetComponent<Dropdown>().options.Add(option3);
    }

    public void onChange()
    {
        int index = GetComponent<Dropdown>().value;
        selection = GetComponent<Dropdown>().captionText.text; //This is not working until I select Option 3 first
        Debug.Log("selected something different at index " + index + ": " + selection);
    }
}
