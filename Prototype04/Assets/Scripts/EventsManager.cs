﻿using System;

namespace Prototype04.Events
{
	public class EventsManager
	{
		private static event Action								PowerupCollected;

		public static void SubscribeToEvent(Prototype04Event evt , Action actionFunction)
		{
			switch(evt)
			{
				case Prototype04Event.PowerupCollected:
					PowerupCollected += actionFunction;
				return;
			}
		}

		public static void UnsubscribeFromEvent(Prototype04Event evt , Action actionFunction)
		{
			switch(evt)
			{
				case Prototype04Event.PowerupCollected:
					PowerupCollected -= actionFunction;
				return;
			}
		}

		public static void InvokeEvent(Prototype04Event evt)
		{
			switch(evt)
			{
				case Prototype04Event.PowerupCollected:
					PowerupCollected?.Invoke();
				return;
			}
		}
	}
}