﻿using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private int enemiesCount;
    [SerializeField] private int maxEnemies;

    [SerializeField] private MiscDataSO miscDataSO;

    private void Start()
    {
        SpawnEnemyWave(maxEnemies);
        SpawnPowerup();
    }

    private void Update()
    {
        enemiesCount = FindObjectsOfType<Enemy>().Length;

        if(enemiesCount == 0)
        {
            maxEnemies++;
            SpawnEnemyWave(maxEnemies);
            SpawnPowerup();

        }
    }

    private void SpawnEnemyWave(int enemiesToSpawn)
    {
        for(int i = 0; i < enemiesToSpawn; i++)
        {
            Instantiate(miscDataSO.GetEnemyPrefab(), miscDataSO.GetRandomSpawnPosition(), miscDataSO.GetEnemyPrefab().transform.rotation);
        }
    }

    private void SpawnPowerup()
    {
        Instantiate(miscDataSO.GetPowerupPrefab(), miscDataSO.GetRandomSpawnPosition(), miscDataSO.GetPowerupPrefab().transform.rotation);
    }
}
