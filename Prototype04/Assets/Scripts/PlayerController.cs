﻿using Prototype04.Events;
using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private bool hasPowerup = false;
    
    [SerializeField] private GameObject focalPointObj;
    [SerializeField] private GameObject powerupIndicatorObj;
    [SerializeField] private MiscDataSO miscDataSO;
    [SerializeField] private Rigidbody playerBody;

    private void Start()
    {
        SubscribeToEvents();       
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    private void Update()
    {
        float forwardInput = Input.GetAxis("Vertical");

         //Carl D didn't multiply by Time.deltaTime in case you notice difference in speed values
        playerBody.AddForce(focalPointObj.transform.forward * forwardInput * miscDataSO.GetPlayerMoveSpeed() * Time.deltaTime , ForceMode.Impulse);

        if(powerupIndicatorObj.activeSelf)
        {
            powerupIndicatorObj.transform.position = transform.position + new Vector3(0f , -0.4f , 0f);
        }
    }

    private IEnumerator PowerupCountdownRoutine()
    {
        yield return new WaitForSeconds(miscDataSO.GetPlayerPowerupCountdownTime());
        EndPowerup();
    }

    private void EndPowerup()
    {
        hasPowerup = false;
        powerupIndicatorObj.SetActive(false);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Enemy") && hasPowerup) //I used to use collision.gameObject.Tag so this is interesting
        {
            //Consider creating another event for this
            Rigidbody enemyBody = collision.gameObject.GetComponent<Rigidbody>();
            Vector3 awayFromPlayer = collision.gameObject.transform.position - transform.position;
            //FYI Carl D didn't use Time.deltaTime below which is why your power up strength value is 600 while his is 15
            enemyBody.AddForce(awayFromPlayer * miscDataSO.GetPlayerPowerupStrength() * Time.deltaTime , ForceMode.Impulse);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Powerup")) //I used to use other.gameObject.Tag so this is interesting
        {
            EventsManager.InvokeEvent(Prototype04Event.PowerupCollected);
        }
    }

    private void OnCollisionWithPowerup()
    {
        hasPowerup = true;
        powerupIndicatorObj.SetActive(true);
        StartCoroutine(PowerupCountdownRoutine());
    }

//In order to follow Functional Programming, I used my own events so every class is responsible for it's own actions
#region EventListeners
    private void SubscribeToEvents()
    {
        EventsManager.SubscribeToEvent(Prototype04Event.PowerupCollected , OnCollisionWithPowerup);
    }

    private void UnsubscribeFromEvents()
    {
        EventsManager.UnsubscribeFromEvent(Prototype04Event.PowerupCollected , OnCollisionWithPowerup);
    }
}
#endregion EventListeners
