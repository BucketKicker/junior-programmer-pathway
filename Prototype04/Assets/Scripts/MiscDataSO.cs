﻿using UnityEngine;

[CreateAssetMenu]
public class MiscDataSO : ScriptableObject
{
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float enemyMoveSpeed;
    [SerializeField] private float enemyYPosBoundary;
    [SerializeField] private float playerMoveSpeed;
    [SerializeField] private float playerPowerupCountdownRoutineTime;
    [SerializeField] private float playerPowerupStrength;
    [SerializeField] private float spawnManagerSpawnInterval;
    [SerializeField] private float spawnManagerSpawnRange;
    [SerializeField] private float spawnManagerStartDelay;

    [SerializeField] private GameObject spawnManagerEnemyPrefab;
    [SerializeField] private GameObject spawnManagerPowerupPrefab;

    public float GetEnemyMoveSpeed()
    {
        return enemyMoveSpeed;
    }

    public float GetYBoundary()
    {
        return enemyYPosBoundary;
    }

    public float GetPlayerMoveSpeed()
    {
        return playerMoveSpeed;
    }

    public float GetPlayerPowerupCountdownTime()
    {
        return playerPowerupCountdownRoutineTime;
    }

    public float GetPlayerPowerupStrength()
    {
        return playerPowerupStrength;
    }

    public float GetRotationSpeed()
    {
        return rotationSpeed;
    }

    public float GetSpawnInterval()
    {
        return spawnManagerSpawnInterval;
    }

    public float GetStartDelay()
    {
        return spawnManagerStartDelay;
    }

    public GameObject GetEnemyPrefab()
    {
        return spawnManagerEnemyPrefab;
    }
    
    public GameObject GetPowerupPrefab()
    {
        return spawnManagerPowerupPrefab;
    }

    public Vector3 GetRandomSpawnPosition()
    {
        float spawnPosX = Random.Range(-spawnManagerSpawnRange , spawnManagerSpawnRange);
        float spawnPosZ = Random.Range(-spawnManagerSpawnRange , spawnManagerSpawnRange);
        Vector3 randomPos = new Vector3(spawnPosX , 0f , spawnPosZ);
        return randomPos;
    }
}
