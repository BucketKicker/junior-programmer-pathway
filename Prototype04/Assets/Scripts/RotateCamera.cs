﻿using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    [SerializeField] private MiscDataSO miscDataSO;

    private void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        transform.Rotate(Vector3.up , horizontalInput * miscDataSO.GetRotationSpeed() * Time.deltaTime);
    }
}
