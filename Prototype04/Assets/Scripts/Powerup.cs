﻿using Prototype04.Events;
using UnityEngine;

public class Powerup : MonoBehaviour
{
    private void Start()
    {
        SubscribeToEvents();       
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    private void OnCollisionWithPlayer()
    {
        Destroy(gameObject);
    }

    //In order to follow Functional Programming, I used my own events so every class is responsible for it's own actions
    #region
    private void SubscribeToEvents()
    {
        EventsManager.SubscribeToEvent(Prototype04Event.PowerupCollected , OnCollisionWithPlayer);
    }

    private void UnsubscribeFromEvents()
    {
        EventsManager.UnsubscribeFromEvent(Prototype04Event.PowerupCollected , OnCollisionWithPlayer);
    }
    #endregion
}
