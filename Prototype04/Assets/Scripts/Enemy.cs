﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private GameObject playerObj;
    [SerializeField] private MiscDataSO miscDataSO;
    [SerializeField] private Rigidbody enemyBody;

    private void Update()
    {
        //Carl D didn't use Time.deltaTime which is why your speed value is lot larger than his
        Vector3 lookDirection = (playerObj.transform.position - transform.position).normalized;
        enemyBody.AddForce(lookDirection * miscDataSO.GetEnemyMoveSpeed() * Time.deltaTime);
        
        if(transform.position.y < miscDataSO.GetYBoundary())
        {
            Destroy(gameObject);
        }
    }
}
