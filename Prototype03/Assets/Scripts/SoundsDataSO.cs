﻿using UnityEngine;

[CreateAssetMenu]
public class SoundsDataSO : ScriptableObject
{
    [SerializeField] private AudioClip crashSound;
    [SerializeField] private AudioClip jumpSound;

    public AudioClip GetCrashSound()
    {
        return crashSound;
    }

    public AudioClip GetJumpSound()
    {
        return jumpSound;
    }
}
