﻿using UnityEngine;

[CreateAssetMenu]
public class MiscDataSO : ScriptableObject
{
    private bool isGameOver = false;

    [SerializeField] private float moveLeftSpeed;
    [SerializeField] private float obstacleLeftBoundary;
    [SerializeField] private float spawnManagerStartDelay;
    [SerializeField] private float spawnManagerSpawnInterval;
    [SerializeField] private GameObject spawnManagerObstaclePrefab;
    [SerializeField] private Vector3 spawnManagerSpawnPos;

    public bool IsGameOver()
    {
        return isGameOver;
    }

    public float GetLeftBoundary()
    {
        return obstacleLeftBoundary;
    }

    public float GetSpeed()
    {
        return moveLeftSpeed;
    }

    public float GetStartDelay()
    {
        return spawnManagerStartDelay;
    }

    public float GetSpawnInterval()
    {
        return spawnManagerSpawnInterval;
    }

    public GameObject GetObstaclePrefab()
    {
        return spawnManagerObstaclePrefab;
    }

    public Vector3 GetSpawnPos()
    {
        return spawnManagerSpawnPos;
    }

    public void SetGameOver(bool gameOver)
    {
        isGameOver = gameOver;
    }
}
