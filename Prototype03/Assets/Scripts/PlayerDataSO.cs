﻿using UnityEngine;

[CreateAssetMenu]
public class PlayerDataSO : ScriptableObject
{
    private bool isGrounded;

    [SerializeField] private float[] animationValues;
    [SerializeField] private float gravityModifier;
    [SerializeField] private float jumpForce;

    public bool IsGrounded()
    {
        return isGrounded;
    }

    public float[] GetAnimationValues()
    {
        return animationValues;
    }

    public float GetGravityModifier()
    {
        return gravityModifier;
    }

    public float GetJumpForce()
    {
        return jumpForce;
    }

    public void SetGroundedStatus(bool grounded) //Not sure this is the right way to do it Conceptually because the data here is being changed by some other class
    {
        isGrounded = grounded;
    }
}
