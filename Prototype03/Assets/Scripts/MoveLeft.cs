﻿using UnityEngine;

public class MoveLeft : MonoBehaviour
{   
    [SerializeField] private MiscDataSO miscDataSO;
    private void Update()
    {
        if(!miscDataSO.IsGameOver())
        {
            transform.Translate(Vector3.left * miscDataSO.GetSpeed() * Time.deltaTime);
        }

        if(transform.position.x < miscDataSO.GetLeftBoundary() && gameObject.CompareTag("Obstacle"))
        {
            Destroy(gameObject);
        }
    }
}
