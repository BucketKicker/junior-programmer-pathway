﻿using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private MiscDataSO miscDataSO;
    private void Start()
    {
        InvokeRepeating("SpawnObstacle" , miscDataSO.GetStartDelay() , miscDataSO.GetSpawnInterval());
    }

    private void SpawnObstacle()
    {
        if(!miscDataSO.IsGameOver())
        {
            Instantiate(miscDataSO.GetObstaclePrefab() , miscDataSO.GetSpawnPos() , miscDataSO.GetObstaclePrefab().transform.rotation);
        }
    }
}
