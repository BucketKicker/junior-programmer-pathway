﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private MiscDataSO miscDataSO;
    [SerializeField] private ParticleSystem dirtParticles;
    [SerializeField] private ParticleSystem explosionParticles;
    [SerializeField] private PlayerDataSO playerDataSO;
    [SerializeField] private Rigidbody playerBody;
    [SerializeField] private SoundManager soundManager;
    [SerializeField] private SoundsDataSO soundsDataSO;

    private void Start()
    {
        miscDataSO.SetGameOver(false);
        Physics.gravity *= playerDataSO.GetGravityModifier();
    }

    private void Update()
    {
        if(!miscDataSO.IsGameOver())
        {
            animator.SetFloat("Speed_f" , playerDataSO.GetAnimationValues()[1]);
        }

        if(miscDataSO.IsGameOver())
        {
            dirtParticles.Stop();
        }

        if(Input.GetKeyDown(KeyCode.LeftControl))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        if(Input.GetKeyDown(KeyCode.Space) && playerDataSO.IsGrounded() && !miscDataSO.IsGameOver())
        {
            animator.SetTrigger("Jump_trig");
            dirtParticles.Stop();
            playerBody.AddForce(Vector3.up * playerDataSO.GetJumpForce() , ForceMode.Impulse);
            playerDataSO.SetGroundedStatus(false);
            soundManager.GetInGameSoundsSource().PlayOneShot(soundsDataSO.GetJumpSound() , 1.0f);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Ground")) //I used to use Tag.Equals instead of CompareTag so this is interesting
        {
            dirtParticles.Play();
            playerDataSO.SetGroundedStatus(true);
        }
        
        else if(collision.gameObject.CompareTag("Obstacle"))
        {
            animator.SetBool("Death_b" , true);
            animator.SetFloat("Speed_f" , playerDataSO.GetAnimationValues()[0]);
            animator.SetInteger("DeathType_int" , 1);
            
            dirtParticles.Stop();
            explosionParticles.Play();
            
            miscDataSO.SetGameOver(true);

            soundManager.GetInGameSoundsSource().PlayOneShot(soundsDataSO.GetCrashSound() , 1.0f);
        }
    }
}
