﻿using TMPro;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private int wheelsOnGround;

    //Carl talked about [SerializeField] in the Unit 6 which confirms you were doing the right thing
    [SerializeField] private float horizontalInput;
    [SerializeField] private float horsePower;
    [SerializeField] private float rpm;
    [SerializeField] private float speed;
    [SerializeField] private float turnSpeed;
    [SerializeField] private float verticalInput;
    [SerializeField] private GameObject centerOfMassObj;
    [SerializeField] private List<WheelCollider> wheelColliders; 
    [SerializeField] private Rigidbody tankBody;
    [SerializeField] private TextMeshProUGUI rpmText;
    [SerializeField] private TextMeshProUGUI speedometerText;

    //This gets called before Update() and good for the calculations which is very useful
    private void FixedUpdate()
    {
        //The right position of this COM obj on the tank WIP
        tankBody.centerOfMass = centerOfMassObj.transform.position;

        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        if(IsGrounded())
        {
            tankBody.AddRelativeForce(Vector3.forward * horsePower * verticalInput , ForceMode.Impulse);
            transform.Rotate(Vector3.up * horizontalInput * turnSpeed * Time.deltaTime);

            speed = Mathf.RoundToInt(tankBody.velocity.magnitude * 3.6f); //Multiply by 2.237f instead for speed in mph
            rpm = Mathf.Round((speed % 30) * 40);

            rpmText.SetText("RPM : " + rpm);
            speedometerText.SetText("Speed : " + speed + " kmph"); //You normally do speedometerText.text = .... and learned about SetText just now so that's great!!!
        }
    }

    private bool IsGrounded()
    {
        wheelsOnGround = 0;

        foreach(WheelCollider wheelCollider in wheelColliders)
        {
            if(wheelCollider.isGrounded)
            {
                wheelsOnGround++;
            }
        }

        if(wheelsOnGround == 4)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
