﻿using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField] private Vector3 offset;
    //Carl talked about [SerializeField] in the Unit 6 which confirms you were doing the right thing
    [SerializeField] private GameObject player;

    //This is called after FixedUpdate() so all the calculations are done already resulting in smooth movement. Very useful
    private void LateUpdate()
    {
        transform.position = player.transform.position + offset;
    }
}
