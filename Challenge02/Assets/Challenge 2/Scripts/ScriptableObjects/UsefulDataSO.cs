﻿using UnityEngine;

[CreateAssetMenu]
public class UsefulDataSO : ScriptableObject
{
    [SerializeField] private float destroyOutOfBoundsBottomLimit;
    [SerializeField] private float destroyOutOfBoundsLeftLimit;
    [SerializeField] private float spawnManagerSpawnStartDelay;
    [SerializeField] private float spawnManagerLimitXLeft;
    [SerializeField] private float spawnManagerLimitXRight;
    [SerializeField] private float spawnManagerSpawnInterval;
    [SerializeField] private float spawnManagerSpawnPositionY;

    [SerializeField] private GameObject[] ballPrefabs;
    [SerializeField] private GameObject dogPrefab;

    [SerializeField] private int playerControllerDogsCount;
    [SerializeField] private int playerControllerMaxDogs;

    public float GetBottomLimit()
    {
        return destroyOutOfBoundsBottomLimit;
    }

    public float GetLeftLimit()
    {
        return destroyOutOfBoundsLeftLimit;
    }

    public int GetMaxDogs()
    {
        return playerControllerMaxDogs;
    }

    public float GetSpawnInterval()
    {
        return spawnManagerSpawnInterval;
    }

    public float GetSpawnLimitXLeft()
    {
        return spawnManagerLimitXLeft;
    }

    public float GetSpawnLimitXRight()
    {
        return spawnManagerLimitXRight;
    }

    public float GetSpawnPositionY()
    {
        return spawnManagerSpawnPositionY;
    }

    public float GetSpawnStartDelay()
    {
        return spawnManagerSpawnStartDelay;
    }

    public GameObject[] GetBalls()
    {
        return ballPrefabs;
    }

    public GameObject GetDog()
    {
        return dogPrefab;
    }

    public int GetDogsCount()
    {
        return playerControllerDogsCount;
    }
}
