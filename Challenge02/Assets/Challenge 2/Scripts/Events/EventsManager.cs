using System;

namespace Game.Events
{   
    public class EventsManager
    {
        private static event Action             BallInactiveAction;
		private static event Action             DogInactiveAction;

		public static void SubscribeToEvent(GameEvent evt , Action actionFunction)
		{
			switch(evt)
			{
				case GameEvent.BallInactive:
					BallInactiveAction += actionFunction;
				return;

				case GameEvent.DogInactive:
					DogInactiveAction += actionFunction;
				return;
			}
		}

		public static void UnsubscribeFromEvent(GameEvent evt , Action actionFunction)
		{
			switch(evt)
			{
				case GameEvent.BallInactive:
					BallInactiveAction -= actionFunction;
				return;

				case GameEvent.DogInactive:
					DogInactiveAction -= actionFunction;
				return;
			}
		}

		public static void Invoke(GameEvent evt)
		{
			switch(evt)
			{
				case GameEvent.BallInactive:
					BallInactiveAction?.Invoke();
				return;

				case GameEvent.DogInactive:
					DogInactiveAction?.Invoke();
				return;
			}
		}
    }
}
