﻿using Game.Events;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
    private int _dogsCount;
    private int _maxDogsCount;

    [SerializeField] private ObjectPool objectPool;
    [SerializeField] private UsefulDataSO usefulDataSO;

    private void Start()
    {
        _dogsCount = usefulDataSO.GetDogsCount();   
        _maxDogsCount = usefulDataSO.GetMaxDogs();
        SubscribeToEvents();
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(_dogsCount < _maxDogsCount)
            {
                GameObject dog = objectPool.GetAvailableDog();
                dog.SetActive(true);
                _dogsCount++;
            }
        }
    }

    private void OnDogInactive()
    {
        _dogsCount--;
    }

    #region Event Listeners
    private void SubscribeToEvents()
    {
        EventsManager.SubscribeToEvent(GameEvent.DogInactive , OnDogInactive);
    }

    private void UnsubscribeFromEvents()
    {
        EventsManager.UnsubscribeFromEvent(GameEvent.DogInactive , OnDogInactive);
    }
    #endregion
}
