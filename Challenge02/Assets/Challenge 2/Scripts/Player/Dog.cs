using Game.Events;
using UnityEngine;

public class Dog : MonoBehaviour
{
    [SerializeField] private Vector3 defaultPosition;
    private void Start()
    {
        defaultPosition = transform.position;
        SubscribeToEvents();
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    private void OnDogInactive()
    {
        transform.position = defaultPosition;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag.Equals("Wall"))
        {
            EventsManager.Invoke(GameEvent.DogInactive);
            gameObject.SetActive(false);
        }
    }

    #region Event Listeners
    private void SubscribeToEvents()
    {
        EventsManager.SubscribeToEvent(GameEvent.DogInactive , OnDogInactive);
    }

    private void UnsubscribeFromEvents()
    {
        EventsManager.UnsubscribeFromEvent(GameEvent.DogInactive , OnDogInactive);
    }
    #endregion
}
