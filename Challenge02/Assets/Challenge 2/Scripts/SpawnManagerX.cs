﻿using UnityEngine;

public class SpawnManagerX : MonoBehaviour
{
    //TODO When you have time, find out why on every spawn, the ball falling speed increases and fix it to remain same every time so the game is playable
    private float randomSpawnInterval;

    [SerializeField] private ObjectPool objectPool;
    [SerializeField] private UsefulDataSO usefulDataSO;

    private void Start()
    {
        randomSpawnInterval = Random.Range(usefulDataSO.GetSpawnInterval() - 1 , usefulDataSO.GetSpawnInterval() + 1);
        InvokeRepeating("SpawnRandomBall" , usefulDataSO.GetSpawnStartDelay() , randomSpawnInterval);
    }

    private void SpawnRandomBall()
    {
        GameObject ball = objectPool.GetAvailableBall();
        randomSpawnInterval = Random.Range(usefulDataSO.GetSpawnInterval() - 1 , usefulDataSO.GetSpawnInterval() + 1);
        
        if(ball != null)
        {
            ball.SetActive(true); // Note that ball will be null if the object from the pool is active in hierarchy
        }
    }
}
