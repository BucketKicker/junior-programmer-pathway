﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] private int ballsPoolDepth;
    [SerializeField] private int dogsPoolDepth;
    [SerializeField] private bool canGrow = true;
    [SerializeField] private UsefulDataSO usefulDataSO;

    private readonly List<GameObject> ballsPool = new List<GameObject>();
    private readonly List<GameObject> dogsPool = new List<GameObject>();

    private void Awake()
    {
        for(int i = 0; i < ballsPoolDepth; i++)
        {
            GameObject pooledBall = Instantiate(usefulDataSO.GetBalls()[i]);
            pooledBall.SetActive(false);
            ballsPool.Add(pooledBall);
        }

        for(int i = 0; i < dogsPoolDepth; i++)
        {
            GameObject pooledDog = Instantiate(usefulDataSO.GetDog());
            pooledDog.SetActive(false);
            dogsPool.Add(pooledDog);
        }
    }

    public GameObject GetAvailableBall()
    {
        int ballsIndex = Random.Range(0 , ballsPoolDepth);

        for(int i = 0; i < ballsPoolDepth; i++)
        {
            if(ballsPool[ballsIndex].activeInHierarchy == false)
                return ballsPool[ballsIndex];
        }

        if(canGrow == true)
        {
            GameObject pooledObject = Instantiate(usefulDataSO.GetBalls()[ballsIndex]);
            ballsPool.Add(pooledObject);

            return pooledObject;
        }

        else
            return null;
    }

    public GameObject GetAvailableDog()
    {
        for(int i = 0; i < ballsPoolDepth; i++)
        {
            if(dogsPool[i].activeInHierarchy == false)
                return dogsPool[i];
        }

        if(canGrow == true)
        {
            GameObject pooledObject = Instantiate(usefulDataSO.GetDog());
            dogsPool.Add(pooledObject);
            return pooledObject;
        }

        else
            return null;
    }
}
