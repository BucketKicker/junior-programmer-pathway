using Game.Events;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private Vector3 defaultPosition;
    private void Start()
    {
        defaultPosition = transform.position;
        SubscribeToEvents();
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    private void OnBallInactive()
    {
        transform.position = defaultPosition;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag.Equals("Ground"))
        {
            EventsManager.Invoke(GameEvent.BallInactive);
            gameObject.SetActive(false);
        }
    }

    #region Event Listeners
    private void SubscribeToEvents()
    {
        EventsManager.SubscribeToEvent(GameEvent.BallInactive , OnBallInactive);
    }

    private void UnsubscribeFromEvents()
    {
        EventsManager.UnsubscribeFromEvent(GameEvent.BallInactive , OnBallInactive);
    }
    #endregion
}
