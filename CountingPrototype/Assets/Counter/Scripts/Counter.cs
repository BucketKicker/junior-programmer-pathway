﻿using UnityEngine;
using UnityEngine.UI;

/**************************************************************************
 * Potential things to come ***********************************************
 * 1. Usage of Text Mesh Pro for UI & 3D Texts ****************************
 * 2. InGameUI & Main Menu Screen *****************************************
 * 3. Scriptable Objects to separate data from MonoBehaviours *************
 * 4. Random Locations for Pegs & Spheres at start ************************
 * ************************************************************************/
public class Counter : MonoBehaviour
{
    public Text CounterText;

    private int Count = 0;

    private void Start()
    {
        Count = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        Count += 1;
        CounterText.text = "Count : " + Count;
    }
}
