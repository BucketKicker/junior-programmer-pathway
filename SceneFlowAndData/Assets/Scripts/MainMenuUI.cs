using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUI : MonoBehaviour
{
    private int _currentScene;

    public int CurrentScene
    {
        get => _currentScene;
        set => _currentScene = value;
    }

    private void Start()
    {
        _currentScene = SceneManager.GetActiveScene().buildIndex; //TODO Make this Persistent
    }

    public void Play()
    {
        SceneManager.LoadScene(_currentScene + 1);
    }

    public void Quit()
    {
        #if UNITY_EDITOR
                EditorApplication.ExitPlaymode(); //This is to test within the Unity
        #else
                Application.Quit(); //This works only in the built application
        #endif
    }
}
