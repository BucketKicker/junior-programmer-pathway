using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour
{
    private int currentLevel;

    [SerializeField] private Color colourToSet;
    [SerializeField] private GameObject blueButtonObj;
    [SerializeField] private GameObject circleObj;
    [SerializeField] private GameObject greenButtonObj;
    [SerializeField] private GameObject menuPanelObj;
    [SerializeField] private GameObject pauseButtonObj;
    [SerializeField] private GameObject redButtonObj;
    [SerializeField] private GameObject setColourButtonObj;

    [SerializeField] private TMP_Text blueSelectedTMP;
    [SerializeField] private TMP_Text greenSelectedTMP;
    [SerializeField] private TMP_Text redSelectedTMP;

    private void Start()
    {
        currentLevel = SceneManager.GetActiveScene().buildIndex;
    }

    public void Blue()
    {
        blueSelectedTMP.enabled = true;
        greenSelectedTMP.enabled = false;
        redSelectedTMP.enabled = false;
        
        colourToSet = blueButtonObj.GetComponent<Image>().color;
        ColourPicker.Instance.currentColour = colourToSet;
    }
    public void Exit()
    {
        ColourPicker.Instance.SaveColor();
        SceneManager.LoadScene(currentLevel - 1);
    }
    
    public void Green()
    {
        blueSelectedTMP.enabled = false;
        greenSelectedTMP.enabled = true;
        redSelectedTMP.enabled = false;
        
        colourToSet = greenButtonObj.GetComponent<Image>().color;
        ColourPicker.Instance.currentColour = colourToSet;
    }

    public void Pause()
    {
        blueButtonObj.SetActive(false);
        circleObj.SetActive(false);
        greenButtonObj.SetActive(false);
        menuPanelObj.SetActive(true);
        pauseButtonObj.SetActive(false);
        redButtonObj.SetActive(false);
        setColourButtonObj.SetActive(false);
    }
    
    public void Red()
    {
        blueSelectedTMP.enabled = false;
        greenSelectedTMP.enabled = false;
        redSelectedTMP.enabled = true;
        
        colourToSet = redButtonObj.GetComponent<Image>().color;
        ColourPicker.Instance.currentColour = colourToSet;
    }

    public void Resume()
    {
        blueButtonObj.SetActive(true);
        circleObj.SetActive(true);
        greenButtonObj.SetActive(true);
        menuPanelObj.SetActive(false);
        pauseButtonObj.SetActive(true);
        redButtonObj.SetActive(true);
        setColourButtonObj.SetActive(true);
    }

    public void SetColour()
    {
        circleObj.GetComponent<SpriteRenderer>().color = colourToSet;
    }
}
