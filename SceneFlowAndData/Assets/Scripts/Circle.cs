using UnityEngine;

public class Circle : MonoBehaviour //OOP 2nd Pillar Inheritance
{
    [SerializeField] private SpriteRenderer circleRenderer;
    private void Start()
    {
        if(ColourPicker.Instance != null)
        {
           ColourPicker.Instance.SetColour(ColourPicker.Instance.currentColour);
           circleRenderer.color = ColourPicker.Instance.currentColour;
        }
        
        Add(1 , 2);
        Add("Function" , " Overloading");
    }

    //Rider suggested to make these functions static so I added it but not sure why
    private static void Add(int a , int b) //OOP 3rd Pillar Polymorphism Function Overloading
    {
        int result = a + b;
        Debug.Log("Result = " + result);
    }

    private static void Add(string a , string b) //OOP 3rd Pillar Polymorphism Function Overloading
    {
        string result = a + b;
        Debug.Log("Result = " + result);
    }
}