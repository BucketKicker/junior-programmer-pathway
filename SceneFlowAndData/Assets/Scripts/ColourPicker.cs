using System.IO;
using TMPro;
using UnityEngine;

public class ColourPicker : MonoBehaviour
{
    public Color currentColour;
    public TMP_Text selectedColourTMPText;

    public static ColourPicker Instance;

    private void Awake()
    {
        if(Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        
        LoadColor();
    }
    public void SetColour(Color colour)
    {
        currentColour = colour;
    }

    [System.Serializable]
    class DataManager //OOP 1st Pillar Abstraction
    {
        public Color selectedColour;
    }

    public void SaveColor()
    {
        DataManager dataManager = new DataManager();
        dataManager.selectedColour = currentColour;

        string json = JsonUtility.ToJson(dataManager);
  
        File.WriteAllText(Application.persistentDataPath + "/savefile.json" , json);
    }

    public void LoadColor()
    {
        string path = Application.persistentDataPath + "/savefile.json";
        
        if(File.Exists(path))
        {
            string json = File.ReadAllText(path);
            DataManager dataManager = JsonUtility.FromJson<DataManager>(json);

            currentColour = dataManager.selectedColour;
        }
    }
}