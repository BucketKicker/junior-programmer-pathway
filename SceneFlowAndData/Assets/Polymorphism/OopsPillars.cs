using System;
using UnityEngine;

namespace Polymorphism
{
    public class OopsPillars : MonoBehaviour
    {
        private void Start()
        {
            Bhanu();
        }

        public virtual void Bhanu() //This is a virtual function so ovveriding is not necessary
        {
            Debug.Log("Oops Pillars : " + " My name is Bhanu");
        }

        //If this was an abstact class, the abstract function has to be overriden as shown below
        //protected abstract void Bhanu();

        //protected override void Bhanu()
        //{
            
        //}
    }
}
