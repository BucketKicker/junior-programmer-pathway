using System;
using UnityEngine;

namespace Polymorphism
{
    public class Overriding : OopsPillars //Oops 3rd Pillar Polymorphism Function Overriding
    {
        private void Start()
        {
            Bhanu();
        }

        public override void Bhanu()
        {
            string text = "Bhanu understands OOPs Pillars";
            Debug.Log("Overriding : " + text);
        }
    }
}
