using UnityEngine;

namespace Polymorphism
{
    public class ScoreManager : MonoBehaviour
    {
        private int score;

        public int Score //Oops 4th Pillar ScoreManager
        {
            get => score;
            set => score = value;
        }
    
        //You can use the above function Score to update or retrieve score which is very useful
        //Example : ScoreManager.Score = 100 & currentScore = Score;
    }
}
