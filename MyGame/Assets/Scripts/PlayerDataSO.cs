﻿using UnityEngine;

[CreateAssetMenu]
public class PlayerDataSO : ScriptableObject
{
    [SerializeField] private float speed;

    public float GetSpeed()
    {
        return speed;
    }
}
