﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float horizontalInput;
    [SerializeField] private float verticalInput;
    [SerializeField] private PlayerDataSO playerDataSO;
    [SerializeField] private Rigidbody playerBody;

    private void Update()
    {
        PlayerMovement();
    }

    private void PlayerMovement()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        //FYI, instructor Carl D didn't use Time.deltaTime which is why speed value 10.0f was enough
        playerBody.AddForce(Vector3.right * horizontalInput * playerDataSO.GetSpeed() * Time.deltaTime);
        playerBody.AddForce(Vector3.forward * verticalInput * playerDataSO.GetSpeed() * Time.deltaTime);
    }
}
