﻿using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManagerX : MonoBehaviour
{
    private float spawnRate = 1.5f;
    private float timerValue;
    private int score;

    //Since the only purpose is to expose this variable in the inspector, I used [SerializeField] instead of public
    //So other classes don't have access to this variable;
    [SerializeField] private GameDataSO gameDataSO;

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI timerText;
    public TextMeshProUGUI gameOverText;
    public bool isGameActive;
    public Button restartButton;
    public GameObject titleScreen;

    private void Update()
    {
        if(isGameActive)
        {
            if(timerValue > 0)
            {
                timerValue -= Time.deltaTime;
                timerText.text = "Timer: " + Mathf.RoundToInt(timerValue).ToString();
            }

            if(timerValue <= 0)
            {
                GameOver();
            }
        }
    }

    public void StartGame(int difficulty)
    {
        spawnRate /= difficulty;
        isGameActive = true;
        StartCoroutine(SpawnTarget());
        score = 0;
        timerValue = 60.0f;
        timerText.text = "Timer: " + timerValue.ToString();
        UpdateScore(0);
        titleScreen.SetActive(false);
    }

    // While game is active spawn a random target
    private IEnumerator SpawnTarget()
    {
        while(isGameActive)
        {
            yield return new WaitForSeconds(spawnRate);

            int index = Random.Range(0 , gameDataSO.GetTargetPrefabs().Length);
                
            Instantiate(gameDataSO.GetTargetPrefabs()[index] , RandomSpawnPosition() , gameDataSO.GetTargetPrefabs()[index].transform.rotation); 
        }
    }

    // Generate a random spawn position based on a random index from 0 to 3
    private Vector3 RandomSpawnPosition()
    {
        float spawnPosX = gameDataSO.GetMinValueX() + (RandomSquareIndex() * gameDataSO.GetSpaceBetweenSquares());
        float spawnPosY = gameDataSO.GetMinValueY() + (RandomSquareIndex() * gameDataSO.GetSpaceBetweenSquares());

        Vector3 spawnPosition = new Vector3(spawnPosX , spawnPosY , 0);
        return spawnPosition;

    }

    // Generates random square index from 0 to 3, which determines which square the target will appear in
    private int RandomSquareIndex()
    {
        return Random.Range(0 , 4);
    }

    // Update score with value from target clicked
    public void UpdateScore(int scoreToAdd)
    {
        score += scoreToAdd;
        scoreText.text = "Score : " + score.ToString();
    }

    // Stop game, bring up game over text and restart button
    public void GameOver()
    {
        gameOverText.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(true);
        isGameActive = false;
    }

    // Restart game by reloading the scene
    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
