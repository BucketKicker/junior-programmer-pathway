﻿using UnityEngine;

[CreateAssetMenu]
public class GameDataSO : ScriptableObject
{
    [SerializeField] private float minValueX;
    [SerializeField] private float minValueY;
    [SerializeField] private float spaceBetweenSquares;
    [SerializeField] private float timeOnScreen;
    [SerializeField] private GameObject[] targetPrefabs;
    
    public float GetMinValueX()
    {
        return minValueX;
    }

    public float GetMinValueY()
    {
        return minValueY;
    }

    public float GetSpaceBetweenSquares()
    {
        return spaceBetweenSquares;
    }

    public float GetTimeOnScreen()
    {
        return timeOnScreen;
    }

    public GameObject[] GetTargetPrefabs()
    {
        return targetPrefabs;
    }
}
