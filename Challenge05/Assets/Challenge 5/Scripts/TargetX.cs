﻿using System.Collections;
using UnityEngine;

public class TargetX : MonoBehaviour
{
    private float timeOnScreen;
    private GameManagerX gameManagerX;

    [SerializeField] private GameDataSO gameDataSO;
    [SerializeField] private GameObject explosionFx;
    [SerializeField] private int pointValue;
    [SerializeField] private Rigidbody rb;
    

    void Start()
    {
        gameManagerX = GameObject.Find("Game Manager").GetComponent<GameManagerX>();
        timeOnScreen = gameDataSO.GetTimeOnScreen();
        transform.position = RandomSpawnPosition(); 
        StartCoroutine(RemoveObjectRoutine()); // begin timer before target leaves screen
    }

    // When target is clicked, destroy it, update score, and generate explosion
    // Change the method below to OnMouseDown from OnMouseEnter to get the right outcome
    private void OnMouseDown()
    {
        if(gameManagerX.isGameActive)
        {
            Destroy(gameObject);
            gameManagerX.UpdateScore(pointValue);
            Explode();
        }        
    }

    // Generate a random spawn position based on a random index from 0 to 3
    private Vector3 RandomSpawnPosition()
    {
        float spawnPosX = gameDataSO.GetMinValueX() + (RandomSquareIndex() * gameDataSO.GetSpaceBetweenSquares());
        float spawnPosY = gameDataSO.GetMinValueY() + (RandomSquareIndex() * gameDataSO.GetSpaceBetweenSquares());

        Vector3 spawnPosition = new Vector3(spawnPosX , spawnPosY , 0);
        return spawnPosition;

    }

    // Generates random square index from 0 to 3, which determines which square the target will appear in
    private int RandomSquareIndex()
    {
        return Random.Range(0 , 4);
    }


    // If target that is NOT the bad object collides with sensor, trigger game over
    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);

        if(other.gameObject.CompareTag("Sensor") && !gameObject.CompareTag("Bad"))
        {
            gameManagerX.GameOver();
        } 

    }

    // Display explosion particle at object's position
    void Explode()
    {
        Instantiate(explosionFx , transform.position , explosionFx.transform.rotation);
    }

    // After a delay, Moves the object behind background so it collides with the Sensor object
    IEnumerator RemoveObjectRoutine()
    {
        yield return new WaitForSeconds(timeOnScreen);

        if(gameManagerX.isGameActive)
        {
            transform.Translate(Vector3.forward * 5 , Space.World);
        }
    }
}
