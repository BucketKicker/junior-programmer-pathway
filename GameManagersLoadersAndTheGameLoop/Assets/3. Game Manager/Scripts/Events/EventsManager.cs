﻿using System;

namespace ThisCourse.Events
{
	public class EventsManager
	{
		private static event Action<GameManager.GameState , GameManager.GameState> GameStateAction;

		public static void SubscribeToEvent(ThisCourseEvent evt , Action<GameManager.GameState , GameManager.GameState> actionFunction)
		{
			switch(evt)
			{
				case ThisCourseEvent.GameStateChangeEvent:
					GameStateAction += actionFunction;
				return;
			}
		}

		public static void UnsubscribeFromEvent(ThisCourseEvent evt , Action<GameManager.GameState , GameManager.GameState> actionFunction)
		{
			switch(evt)
			{
				case ThisCourseEvent.GameStateChangeEvent:
					GameStateAction -= actionFunction;
				return;
			}
		}

		public static void InvokeEvent(ThisCourseEvent evt , GameManager.GameState currentState , GameManager.GameState previousState)
		{
			switch(evt)
			{
				case ThisCourseEvent.GameStateChangeEvent:
					GameStateAction?.Invoke(currentState , previousState);
				return;
			}
		}
	}
}