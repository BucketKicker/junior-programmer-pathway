using UnityEngine;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Animation animation;
    [SerializeField] private AnimationClip fadeInAnimation;
    [SerializeField] private AnimationClip fadeOutAnimation;

    private void Start()
    {
        SubscribeToEvents();
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    private void OnGameStateChanged(GameManager.GameState currentState , GameManager.GameState previousState)
    {
        if(previousState == GameManager.GameState.PREGAME && currentState == GameManager.GameState.RUNNING)
        {
            FadeOut();
        }

        if(previousState != GameManager.GameState.PREGAME && currentState == GameManager.GameState.PREGAME)
        {
            FadeIn();
        }
    }

    public void FadeIn()
    {
        animation.Stop();
        animation.clip = fadeInAnimation;
        animation.Play();
    }

    public void FadeOut()
    {
        animation.Stop();
        animation.clip = fadeOutAnimation;
        animation.Play();
    }

    public void OnFadeInComplete()
    {
        Debug.LogWarning("[MainMenu] Sir Bhanu, Fade In Complete");
        UIManager.Instance.SetDummyCameraActive(true);
    }

    public void OnFadeOutComplete()
    {
        Debug.LogWarning("[MainMenu] Sir Bhanu, Fade Out Complete");
        UIManager.Instance.SetDummyCameraActive(false);
    }

    #region EventListeners
    private void SubscribeToEvents()
    {
        ThisCourse.Events.EventsManager.SubscribeToEvent(ThisCourse.Events.ThisCourseEvent.GameStateChangeEvent , OnGameStateChanged);
    }

    private void UnsubscribeFromEvents()
    {
        ThisCourse.Events.EventsManager.UnsubscribeFromEvent(ThisCourse.Events.ThisCourseEvent.GameStateChangeEvent , OnGameStateChanged);
    }

    #endregion
}
