using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private UnityEngine.UI.Button quitButton;
    [SerializeField] private UnityEngine.UI.Button restartButton;
    [SerializeField] private UnityEngine.UI.Button resumeButton;
    
    private void Start()
    {
        quitButton.onClick.AddListener(HandleQuitButtonClicked);
        restartButton.onClick.AddListener(HandleRestartButtonClicked);
        resumeButton.onClick.AddListener(HandleResumeButtonClicked);
    }

    private void HandleQuitButtonClicked()
    {
        GameManager.Instance.QuitGame();
    }

    private void HandleRestartButtonClicked()
    {
        GameManager.Instance.RestartGame();
    }

    private void HandleResumeButtonClicked()
    {
        GameManager.Instance.TogglePause();
    }
}
