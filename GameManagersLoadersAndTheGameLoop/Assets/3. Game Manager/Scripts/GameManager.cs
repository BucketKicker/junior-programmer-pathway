using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public enum GameState //When I left it as just enum as instructed, I get an inconsistent access error so changed this to public for now
    {
        PAUSED ,
        PREGAME ,
        RUNNING
    }

    private GameState currentGameState = GameState.PREGAME;

    private System.Collections.Generic.List<AsyncOperation> _loadOperations; //Declared C++ Explicit Style without using System.Collections.Generic at the top
    private System.Collections.Generic.List<GameObject> _instancedSystemPrefabs; //Declared C++ Explicit Style without using System.Collections.Generic at the top
    private string _currentLevelName = string.Empty;

    [SerializeField] private GameObject[] systemPrefabs;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        _loadOperations = new System.Collections.Generic.List<AsyncOperation>();
        InstantiateSystemPrefabs();
    }

    private void Update()
    {
        if(currentGameState == GameState.PREGAME)
        {
            return;
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            ThisCourse.Events.EventsManager.InvokeEvent(ThisCourse.Events.ThisCourseEvent.GameStateChangeEvent , GameState.PAUSED , GameState.RUNNING);
            TogglePause();
        }
    }

    private void InstantiateSystemPrefabs()
    {
        GameObject prefabInstance;

        for(int i = 0; i < systemPrefabs.Length; i++)
        {
            prefabInstance = Instantiate(systemPrefabs[i]);
            _instancedSystemPrefabs.Add(prefabInstance); //TODO Not sure why NullReferenceException so debug when you have time
        }
    }

    private void OnLoadOperationComplete(AsyncOperation ao)
    {
        if(_loadOperations.Contains(ao))
        {
            _loadOperations.Remove(ao);

            if(_loadOperations.Count == 0)
            {
                UpdateGameState(GameState.RUNNING);
            }
        }

        Debug.Log("Load Complete");
    }

    private void OnUnloadOperationComplete(AsyncOperation ao)
    {
        Debug.Log("Unload Complete");
    }

    private void UpdateGameState(GameState state)
    {
        currentGameState = state;

        switch(currentGameState)
        {
            case GameState.PAUSED:
                Time.timeScale = 0;
            break;

            case GameState.PREGAME:
                Time.timeScale = 1;
            break;

            case GameState.RUNNING:
                Time.timeScale = 1;
            break;

            default:
            
            break;
        }
    }

    public GameState CurrentGameState
    {
        get
        {
            return currentGameState;
        }

        private set
        {
            currentGameState = value;
        }
    }

    public void LoadLevel(string levelName)
    {
        //LoadScene means everything must wait until this happens which could lead to some glitches and LoadSceneAsync will do it's thing while others still do their stuff simultaneously
        AsyncOperation ao = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(levelName , UnityEngine.SceneManagement.LoadSceneMode.Additive); //C++ Explicit Style without using Unity.SceneManagement at the top

        if(ao == null)
        {
            Debug.LogError("[Game Manager] Sir Bhanu, Unable to load level" + levelName);
            return;
        }

        ao.completed += OnLoadOperationComplete;
        _loadOperations.Add(ao);
        _currentLevelName = levelName;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void RestartGame()
    {
        UpdateGameState(GameState.PREGAME);
    }

    public void StartGame()
    {
        LoadLevel("Main");
    }

    public void TogglePause()
    {
        UpdateGameState(CurrentGameState == GameState.RUNNING ? GameState.PAUSED : GameState.RUNNING);
    }

    public void UnloadLevel(string levelName)
    {
        AsyncOperation ao = UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(levelName); //C++ Explicit Style without using Unity.SceneManagement at the top
        
        if(ao == null)
        {
            Debug.LogError("[Game Manager] Sir Bhanu, Unable to unload level" + levelName);
            return;
        }

        ao.completed += OnUnloadOperationComplete;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        for(int i = 0; i < _instancedSystemPrefabs.Count; i++) //TODO Not sure why NullReferenceException so debug when you have time
        {
            Destroy(_instancedSystemPrefabs[i]);
        }

        _instancedSystemPrefabs.Clear(); //This will clear all the references
    }
}
