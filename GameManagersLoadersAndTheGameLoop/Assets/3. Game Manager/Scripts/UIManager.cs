using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] private Camera dummyCamera;
    [SerializeField] private MainMenu mainMenu;
    [SerializeField] private PauseMenu pauseMenu;

    private void Start()
    {
        SubscribeToEvents();
    }

    protected override void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    private void Update()
    {
        if(GameManager.Instance.CurrentGameState != GameManager.GameState.PREGAME)
        {
            return;
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            ThisCourse.Events.EventsManager.InvokeEvent(ThisCourse.Events.ThisCourseEvent.GameStateChangeEvent , GameManager.GameState.RUNNING , GameManager.GameState.PREGAME);
        }
    }

    private void OnGameStateChanged(GameManager.GameState currentState , GameManager.GameState previousState)
    {
        if(previousState == GameManager.GameState.RUNNING && currentState == GameManager.GameState.PAUSED)
        {
            pauseMenu.gameObject.SetActive(currentState == GameManager.GameState.PAUSED); //TODO This is not turning the PauseMenu back to inactive when game is resumed
        }
    }

    public void SetDummyCameraActive(bool active)
    {
        dummyCamera.gameObject.SetActive(active);
    }

    #region EventListeners
    private void SubscribeToEvents()
    {
        ThisCourse.Events.EventsManager.SubscribeToEvent(ThisCourse.Events.ThisCourseEvent.GameStateChangeEvent , OnGameStateChanged);
    }

    private void UnsubscribeFromEvents()
    {
        ThisCourse.Events.EventsManager.UnsubscribeFromEvent(ThisCourse.Events.ThisCourseEvent.GameStateChangeEvent , OnGameStateChanged);
    }

    #endregion
}
