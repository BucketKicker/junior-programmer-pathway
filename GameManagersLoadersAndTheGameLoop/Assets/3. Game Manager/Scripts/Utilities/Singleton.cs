using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T> //This concept is Template of C++
{
    private static T instance;

    public static bool IsInitialised
    {
        get
        {
            return instance != null;
        }
    }

    public static T Instance
    {
        get
        {
            return instance;
        }
    }

    protected virtual void Awake() //So this can be accessed by a child of this class and can be overriden
    {
        if(instance != null)
        {
            Debug.LogError("[Singleton] Sir Bhanu, you shouldn't try to instantiate a second instance of a singleton class");
        }
        else
        {
            instance = (T)this;
        }
    }

    protected virtual void OnDestroy() //So this can be accessed by a child of this class and can be overriden
    {
        if(instance == this)
        {
            instance = null;
        }
    }
}
