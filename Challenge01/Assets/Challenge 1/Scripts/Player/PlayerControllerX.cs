﻿using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
    [SerializeField] private BhanuPlanePropeller bhanuPlanePropeller;
    [SerializeField] private BhanuPropellerDataSO bhanuPropellerDataSO;
    [SerializeField] private float horizontalInput;
    [SerializeField] private float speed;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float verticalInput;

    private void Update()
    {
        if(bhanuPlanePropeller.IsPropellerStarted())
        {
            horizontalInput = Input.GetAxis("Horizontal");
            verticalInput = Input.GetAxis("Vertical");
            transform.Translate(Vector3.forward * horizontalInput * speed * Time.deltaTime);
            transform.Rotate(Vector3.left * rotationSpeed * verticalInput * Time.deltaTime);
        }
    }
}
