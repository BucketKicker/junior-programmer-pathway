﻿using Game.Events;
using UnityEngine;

public class BhanuPlanePropeller : MonoBehaviour
{
    private bool propellerStarted;

    [SerializeField] private BhanuPropellerDataSO bhanuPropellerDataSO;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float zAngle;

    private void Start()
    {
        propellerStarted = bhanuPropellerDataSO.IsPropellerStarted();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if(!bhanuPropellerDataSO.IsPropellerStarted())
            {
                propellerStarted = true;
                EventsManager.Invoke(GameEvent.GameStarted);
            }
        }

        if(propellerStarted)
        {
            transform.Rotate(0 , 0 , zAngle * rotationSpeed * Time.deltaTime);
        }
    }

    public bool IsPropellerStarted()
    {
        return propellerStarted;
    }
}
