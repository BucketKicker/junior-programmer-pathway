﻿using Game.Events;
using UnityEngine;

public class FollowPlayerX : MonoBehaviour
{
    private bool _gameStarted;
    private Vector3 offset = new Vector3(15 , 3 , 2);

    [SerializeField] private GameObject plane;

    private void Start()
    {
        _gameStarted = false;
        SubscribeToEvents();
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    private void Update()
    {
        if(_gameStarted) //The 3D Text is laid in front of the plane but when the game starts, the camera comes forward and the text appears behind the plane which is why added _gameStarted check
        {
            transform.position = plane.transform.position + offset;
        }
    }

    private void OnGameStarted()
    {
        _gameStarted = true;
    }

    #region Event Listeners

    private void SubscribeToEvents()
    {
        EventsManager.SubscribeToEvent(GameEvent.GameStarted , OnGameStarted);
    }

    private void UnsubscribeFromEvents()
    {
        EventsManager.UnsubscribeFromEvent(GameEvent.GameStarted , OnGameStarted);
    }

    #endregion
}
