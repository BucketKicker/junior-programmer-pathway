using Game.Events;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject _tutorialTextObj;
    private void Start()
    {
        SubscribeToEvents();
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }

    private void OnGameStarted()
    {
        _tutorialTextObj.SetActive(false);
    }

    #region Event Listeners

    private void SubscribeToEvents()
    {
        EventsManager.SubscribeToEvent(GameEvent.GameStarted , OnGameStarted);
    }

    private void UnsubscribeFromEvents()
    {
        EventsManager.UnsubscribeFromEvent(GameEvent.GameStarted , OnGameStarted);
    }

    #endregion
}
