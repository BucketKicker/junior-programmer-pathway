﻿using UnityEngine;

[CreateAssetMenu]
public class BhanuPropellerDataSO : ScriptableObject
{
    private bool propellerStarted;

    public bool IsPropellerStarted()
    {
        propellerStarted = false;
        return propellerStarted;
    }
}
