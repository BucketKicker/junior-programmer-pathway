using System;

namespace Game.Events
{   
    public class EventsManager
    {
        private static event Action             GameStartAction;

		public static void SubscribeToEvent(GameEvent evt , Action actionFunction)
		{
			switch(evt)
			{
				case GameEvent.GameStarted:
					GameStartAction += actionFunction;
				return;
			}
		}

		public static void UnsubscribeFromEvent(GameEvent evt , Action actionFunction)
		{
			switch(evt)
			{
				case GameEvent.GameStarted:
					GameStartAction -= actionFunction;
				return;
			}
		}

		public static void Invoke(GameEvent evt)
		{
			switch(evt)
			{
				case GameEvent.GameStarted:
					GameStartAction?.Invoke();
				return;
			}
		}
    }
}
